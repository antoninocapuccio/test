import cv2
import numpy as np
from projection import Homography


#1 Acquisisco i frames video con OpenCV -> salvo il 300° frame dove sono visibili tutti reference points
def save_one_frame_from_video():
    cv2.namedWindow("Frame")
    cap = cv2.VideoCapture('VIDEOS/SocialDistancing.mp4')
    i=0
    while cap.isOpened():
        _, frame = cap.read()
        H, W = frame.shape[:2]  #H=480 W=640
        #frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('frame',frame)
        if i==300:
            cv2.imwrite('frame_4_calibration'+'.jpg', frame)
        i+=1
        if cv2.waitKey(1) == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()


#2 Strumento per trovare gli image points con click del mouse
def find_img_points_with_click(frame):
    a = []
    b = []
    def on_EVENT_LBUTTONDOWN(event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            xy = "%d,%d" % (x, y)
            a.append(x)
            b.append(y)
            cv2.circle(frame, (x, y), 1, (0, 0, 255), thickness=-1)
            cv2.putText(frame, xy, (x, y), cv2.FONT_HERSHEY_PLAIN,
                        1.0, (0, 0, 0), thickness=1)
            cv2.imshow("image", frame)
            print(x, y)

    cv2.namedWindow("image")
    cv2.setMouseCallback("image", on_EVENT_LBUTTONDOWN)
    cv2.imshow("image", frame)
    cv2.waitKey(0)
    print(a[0], b[0])


#CALIBRAZIONE USANDO image_points e relativi object_points
def calibration(frame):
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    image_points = np.array([  #[245, 415],  # RP_3
                               [332, 383],  # RP_4
                               [441, 422],  # RP_5
                               [299, 451],  # T1
                               [374, 454],  # T2
                               [453, 450],  # T3
                               [331, 408],  # T4
                               [370, 408],  # T5
                               [430, 407],  # T6
                               [366, 383],  # T7
                               [414, 381]], # T8
                              dtype=np.float32).reshape(1, 10, 2)

    #object_points = np.array([  [4.6, 1, 0.0 ],    # RP_3
    #                            [5.65, 0.0, 0.0],  # RP_4
    #                            [4.75, 0.9, 0.0],  # RP_5
    #                            [4.25, -0.3, 0.0], # T1
    #                            [4.25, 0.3, 0.0],  # T2
    #                            [4.25, 0.9, 0.0],  # T3
    #                            [4.85, -0.3, 0.0], # T4
    #                            [4.85, 0.3, 0.0],  # T5
    #                            [4.85, 0.9, 0.0],  # T6
    #                            [5.65, 0.3, 0.0],  # T7
    #                            [5.65, 0.9, 0.0]], # T8
    #                           dtype=np.float32).reshape(1, 11, 3)

    object_points = np.array([  #[4.6, 1, 0.0 ],    # RP_3
                                [5.65, 0.0, 0.0],  # RP_4
                                [4.75, 0.9, 0.0],  # RP_5
                                [4.45, -0.3, 0.0], # T1
                                [4.45, 0.3, 0.0],  # T2
                                [4.45, 0.9, 0.0],  # T3
                                [5.05, -0.3, 0.0], # T4
                                [5.05, 0.3, 0.0],  # T5
                                [5.05, 0.9, 0.0],  # T6
                                [5.65, 0.3, 0.0],  # T7
                                [5.65, 0.9, 0.0]], # T8
                               dtype=np.float32).reshape(1, 10, 3)
    W=frame_gray.shape[1]
    H=frame_gray.shape[0]
    _, camera_Matrix, distCoeffs, rvecs, tvecs = cv2.calibrateCamera(object_points, image_points, (W,H), None, None)  #oppure frame_gray.shape[::-1]

    return  camera_Matrix, distCoeffs, rvecs[0], tvecs[0]



frame= cv2.imread('frame_4_calibration.jpg')

#find_img_points_with_click(frame)

camera_Matrix, distCoeffs, rvecs, tvecs= calibration(frame)

#Calcolo posa reale:
homography = Homography(camera_Matrix, rvecs, tvecs)
u=299
v=451
person_xy=homography.uv_to_world([[u], [v], [1]])[:2]
print(person_xy)
