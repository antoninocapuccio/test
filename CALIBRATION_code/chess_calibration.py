import numpy as np
import cv2
import yaml
from projection import Homography



#1) CALCOLO IMAGE POINTS E OBJECTS POINTS USANDO FRAME CON CHESSBOARD PRESENTE----------------------------------

#Prepare object points, like (0,0,0), (1,0,0), (2,0,0) ...dispozione: fisso colonna (x) e varia riga (y) (GRIGLIA 9 righe X 6 colonne ; punti = corner di box neri contigui)
objp = np.zeros((6*9,3), np.float32)
objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)
#Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

#Carico il frame
found =0  #tiene conto dei frame processati
img = cv2.imread('last_frame.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#Find the chess board corners= img points (nel sistema di riferimento classico del frame: origine angolo alto sinistro; x da sx a dx / y da top a botton)
ret, corners = cv2.findChessboardCorners(gray, (9,6), None)

#If found, add object points, image points (after refining them)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
if ret == True:
    objpoints.append(objp)
    corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)  #funzione che migliora l'accuratezza
    imgpoints.append(corners2)

    # Draw and display the corners
    img = cv2.drawChessboardCorners(img, (9,6), corners2, ret)
    found += 1
    cv2.imshow('img', img)
    cv2.waitKey(1000)
    #Salvo l'immagine con gli img points
    cv2.imwrite('calibrated_frame.jpg', img)

print("Number of images used for calibration: ", found)
cv2.destroyAllWindows()




#2) CALIBRAZIONE e RE-PROJECTION ERROR------------------------------------------------------------------
_, camera_Matrix, coeff_dist, r_vecs, t_vecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

#Salvo risultati in file.yaml
data = {'camera_matrix': np.asarray(camera_Matrix).tolist(), 'dist_coeff': np.asarray(coeff_dist).tolist()}
with open("calibration_data.yaml", "w") as f:
    yaml.dump(data, f)

#Calcolo errore di calibrazione (migliore tanto più vicino a 0)
mean_error = 0
for i in range(len(objpoints)):
    imgpoints2, _ = cv2.projectPoints(objpoints[i], r_vecs[i], t_vecs[i], camera_Matrix, coeff_dist)
    error = cv2.norm(imgpoints[i], imgpoints2, cv2.NORM_L2)/len(imgpoints2)
    mean_error += error
print( "total error: {}".format(mean_error/len(objpoints)) )



#3) Creazione immagine non distorta---------------------------------------------------------------------
img = cv2.imread('chess_frame_4_calibration.jpg')
h, w = img.shape[:2]
newcameramtx, roi = cv2.getOptimalNewCameraMatrix(camera_Matrix, coeff_dist, (w,h), 0, (w,h))  #scaling parameter alpha=1/0

#1° metodo
dst = cv2.undistort(img, camera_Matrix, coeff_dist, None, newcameramtx)
# crop the image
x, y, w, h = roi
img_undistorted1 = dst[y:y+h, x:x+w]
#cv2.imshow('img_undistorted1', img_undistorted1)
#cv2.waitKey(1000)
#cv2.imwrite('img_undistorted1.png', img_undistorted1)

#2°metodo
mapx, mapy = cv2.initUndistortRectifyMap(camera_Matrix, coeff_dist, None, newcameramtx, (w,h), 5)
dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)
# crop the image
x, y, w, h = roi
img_undistorted2= dst[y:y+h, x:x+w]
#cv2.imshow('img_undistorted2', img_undistorted2)
#cv2.waitKey(1000)
#cv2.imwrite('img_undistorted2.png', img_undistorted2)





##################################################################################################




frame= cv2.imread('frame_4_calibration.jpg')                # dimensione diversa, (WxH = 1920x1080)
frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
image_points = np.array([     #[245, 415],  # RP_3
                               [332, 383],  # RP_4
                               [441, 422],  # RP_5
                               [299, 451],  # T1
                               [374, 454],  # T2
                               [453, 450],  # T3
                               [331, 408],  # T4
                               [370, 408],  # T5
                               [430, 407],  # T6
                               [366, 383],  # T7
                               [414, 381]], # T8
                              dtype=np.float32).reshape(10,1, 2)

object_points = np.array([     #[4.6, 1, 0.0 ],    # RP_3
                                [5.65, 0.0, 0.0],  # RP_4
                                [4.75, 0.9, 0.0],  # RP_5
                                [4.45, -0.3, 0.0], # T1
                                [4.45, 0.3, 0.0],  # T2
                                [4.45, 0.9, 0.0],  # T3
                                [5.05, -0.3, 0.0], # T4
                                [5.05, 0.3, 0.0],  # T5
                                [5.05, 0.9, 0.0],  # T6
                                [5.65, 0.3, 0.0],  # T7
                                [5.65, 0.9, 0.0]], # T8
                               dtype=np.float32).reshape(10, 1, 3)

_, r_vecs, t_vecs = cv2.solvePnP(object_points, image_points, camera_Matrix, coeff_dist)


#Calcolo posa reale:
homography = Homography(camera_Matrix, r_vecs, t_vecs)
u=332
v=383
person_xy=homography.uv_to_world([[u], [v], [1]])[:2]
print(person_xy)














