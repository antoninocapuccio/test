#!/usr/bin/env bash

rm /etc/apt/sources.list
ln -s /etc/apt/sources.list.online /etc/apt/sources.list
apt update
pushd /opt/intel/openvino_2021/install_dependencies/
./install_NEO_OCL_driver.sh  #solo questo
popd
usermod -a -G video evoca  
