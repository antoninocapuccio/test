#!/usr/bin/env bash

apt-get update
wget -O /tmp/GPG-PUB-KEY-INTEL-OPENVINO-2021 -o /tmp/gpg-openvino.log https://apt.repos.intel.com/openvino/2021/GPG-PUB-KEY-INTEL-OPENVINO-2021
apt-key add /tmp/GPG-PUB-KEY-INTEL-OPENVINO-2021
rm /tmp/GPG-PUB-KEY-INTEL-OPENVINO-2021
echo "deb https://apt.repos.intel.com/openvino/2021 all main" | sudo tee /etc/apt/sources.list.d/intel-openvino-2021.list
apt update
apt install -y intel-openvino-runtime-ubuntu18-2021.1.110
apt install -y intel-openvino-dev-ubuntu18-2021.1.110
