#!/usr/bin/env bash

#sudo apt-get install -y python3-venv cmake gcc protobuf-compiler libprotobuf-dev python3-dev
python3 -m venv --system-site-packages /mnt/data/software/latek/venv
source /mnt/data/software/latek/venv/bin/activate
pushd /mnt/data/software/latek
#wget  https://files.pythonhosted.org/packages/24/19/4804aea17cd136f1705a5e98a00618cb8f6ccc375ad8bfa437408e09d058/torch-1.4.0-cp36-cp36m-manylinux1_x86_64.whl
popd
#python3 -mpip download -d /mnt/data/software/latek protobuf
#python3 -mpip download -d /mnt/data/software/latek scipy
#python3 -mpip download -d /mnt/data/software/latek torchvision==0.5.*
#python3 -mpip download -d /mnt/data/software/latek typing-extensions
#python3 -mpip download -d /mnt/data/software/latek wheel
#python3 -mpip download -d /mnt/data/software/latek pybind11
#python3 -mpip download -r ./requirements.in -d /mnt/data/software/latek
#python3 -mpip download -r ./requirements-caffe2.in -d /mnt/data/software/latek
python3 -mpip install --user --find-links="/mnt/data/software/latek" wheel
python3 -mpip install --user --find-links="/mnt/data/software/latek" pybind11
python3 -mpip install --user --find-links="/mnt/data/software/latek" onnx
python3 -mpip install --user --no-index --find-links="/mnt/data/software/latek" -r requirements.in
python3 -mpip install -U wheel --user --no-index --find-links="/mnt/data/software/latek" -r requirements-pytorch.in
python3 -mpip install -U wheel --user --no-index --find-links="/mnt/data/software/latek" -r requirements-caffe2.in
