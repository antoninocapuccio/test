import cv2
import numpy as np


class Homography():                                    #Definisco una classe Homography
    # Converted from C++
    # https://stackoverflow.com/questions/12299870/computing-x-y-coordinate-3d-from-image-point

    def __init__(self, camera_matrix, r_vecs, t_vecs):
        self.t_vecs = t_vecs
        rotation_matrix, _ = cv2.Rodrigues(r_vecs)
        _, self.inv_rot_matrix = cv2.invert(rotation_matrix)
        _, self.inv_camera_matrix = cv2.invert(camera_matrix)
        self.tmp = self.inv_rot_matrix.dot(self.inv_camera_matrix)
        self.right_side_mat = self.inv_rot_matrix.dot(t_vecs)

    def uv_to_world(self, uv_point):                   #Una volta creato un oggetto type "Homography", .uv_toworld sarà un metodo che posso applicare
        left_side_mat = self.tmp.dot(uv_point)         #converte da coordinate frame [pixels] a coordinate reali [metri]
        s = self.right_side_mat[2, 0] / left_side_mat[2, 0]
        return self.inv_rot_matrix.dot(s * self.inv_camera_matrix.dot(uv_point) - self.t_vecs[0])


def rvec_to_rot_angles(r_vecs, VERBOSE=False):         #Trasforma l'orientazione da vettore rotazione a matrice di rotazione
    rotation_matrix, _ = cv2.Rodrigues(r_vecs)
    _, _, _, Qx, Qy, Qz = cv2.RQDecomp3x3(rotation_matrix)
    if VERBOSE:
        print("alpha = {}".format(np.arccos(Qx[1, 1]) / np.pi * 180))
        print("beta = {}".format(np.arccos(Qy[0, 0]) / np.pi * 180))
        print("gamma = {}".format(np.arccos(Qz[0, 0]) / np.pi * 180))